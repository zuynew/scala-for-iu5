/// sub typing

trait PlusIntf[A] {
  def plus(a2: A): A
}

def plusBySubtype[A <: PlusIntf[A]](a1: A, a2: A): A = a1.plus(a2)

class VeryRichInt(val i: Int) extends PlusIntf[VeryRichInt] {
  override def plus(a2: VeryRichInt): VeryRichInt = new VeryRichInt(i + a2.i)

  override def toString = s"VeryRichInt($i)"
}

plusBySubtype(new VeryRichInt(1), new VeryRichInt(2))
// res0: VeryRichInt = VeryRichInt(3)



// ad-hoc (typeclasses)
trait CanPlus[A] {
  def plus(a1: A, a2: A): A
}

def plus[A: CanPlus](a1: A, a2: A): A = implicitly[CanPlus[A]].plus(a1, a2)

implicit val intCanPlus: CanPlus[Int] = new CanPlus[Int] {
  override def plus(a1: Int, a2: Int): Int = a1 + a2
}

plus(1, 2)
// res1: Int = 3



