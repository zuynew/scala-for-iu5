case class Fraction(numerator: Int, denominator: Int)
{
  def * (that: Fraction) = Fraction(
    that.numerator*numerator,
    that.denominator*denominator
  )
}

implicit def int2Fraction(i: Int): Fraction = Fraction(i, 1)

2 * Fraction(3, 4)
// res0: Fraction = Fraction(6,4)

def pow(i: Int, power: Int): Int = {
  def loop0(j: Int, acc: Int): Int = {
    if (j == 0)
    {
      acc
    }
    else
    {
      loop0(j-1, acc * i)
    }
  }
  loop0(power, 1)
}

class VeryRichInt(val i: Int)
{
  def **(that: Int) = pow(i, that)
}


implicit def int2VeryRichInt(i: Int): VeryRichInt = new VeryRichInt(i)


implicit class VeryVeryRichInt(i: Int)
{
  def ***(that: Int) = pow(i, that)
}

10 ** 2
// res1: Int = 100
10 *** 3
// res2: Int = 1000


case class Delimiters(left: String, right: String)

def quote0(what: String)
          (implicit delimiters: Delimiters): String =
  delimiters.left + what + delimiters.right

implicit class QuoteSyntax(str: String)
{
  def quote()
           (implicit delimiters: Delimiters): String = quote0(str)
}

implicit val backticks = Delimiters("```", "```")

quote0("Foo")
// res1: String = ```Foo```

"Bar".quote()
// res2: String = ```Bar```