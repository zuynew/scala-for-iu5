

class Foo {
  def apply(): String = "Foo instance apply called"
}

object Foo {
  def apply(): Foo = new Foo()
}

val foo: Foo = Foo()
// foo: Foo

foo()
// res0: String = Foo instance apply called


// ----

case class Calculator(brand: String, model: String)

val hp20b = Calculator("HP", "20B")
val hp30b = Calculator("HP", "30B")

def calcType(calc: Calculator) = calc match {
  case Calculator("HP", "20B") => "financial"
  case Calculator("HP", "48G") => "scientific"
  case Calculator("HP", "30B") => "business"
  case Calculator(ourBrand, ourModel) => s"Calculator: $ourBrand $ourModel is of unknown type"
}

class Calculator0(val brand: String, val model: String)
{

  def canEqual(other: Any): Boolean = ???

  override def equals(other: Any): Boolean = ???

  override def hashCode(): Int = ???

  def copy(brand: String = this.brand, model: String = this.model)
  = new Calculator0(brand, model)

  override def toString = s"Calculator0($brand, $model)"
}

object Calculator0
{
  def apply(brand: String, model: String): Calculator0 = new Calculator0(brand, model)

  def unapply(arg: Calculator0): Option[(String, String)] = Some((arg.brand, arg.model))

}

// ---

sealed trait Tree[T]
case class Node[T](left: Tree[T], right: Tree[T]) extends Tree[T]
case class Leaf[T](value: T) extends Tree[T]

val tree: Tree[Int] = Node(Node(Leaf(14), Leaf(88)), Leaf(99))
// tree: Tree[Int] = Node(Node(Leaf(14),Leaf(88)),Leaf(99))

import scala.math.Ordering

def findMin[T : Ordering](tree: Tree[T]): T = tree match {
  case Node(left, right) => Seq(findMin(left), findMin(right)).min
  case Leaf(value) => value
}

findMin(tree)
// res1: Int = 14
