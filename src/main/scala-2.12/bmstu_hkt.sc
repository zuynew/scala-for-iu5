trait Container[M[_]] {
  def put[A](x: A): M[A]
  def get[A](m: M[A]): A
}

implicit val listContainer = new Container[List] {
  def put[A](x: A) = List(x)
  def get[A](m: List[A]) = m.head
}

listContainer.put("Foo")
//res0: List[String] = List(Foo)
listContainer.get(List("Foo", "Bar"))
//res1: String = Foo

type Id[A] = A

implicit val idContainer = new Container[Id] {
  def put[A](x: A) = x
  def get[A](m: Id[A]) = m
}

idContainer.put("Foo")
//res2: Id[String] = Foo
idContainer.get("Bar": Id[String])
// res3: String = Bar


def tupleize[M[_]: Container, A, B](fst: M[A], snd: M[B]) = {
  val c = implicitly[Container[M]]
  c.put(c.get(fst), c.get(snd))
}

def tupleize0[M[_], A, B](fst: M[A], snd: M[B])(
  implicit c: Container[M]) = {
  c.put(c.get(fst), c.get(snd))
}

tupleize(List(1), List(2))
// res4: List[(Int, Int)] = List((1,2))

tupleize(1: Id[Int], 2: Id[Int])
// res4: Id[(Int, Int)] = (1,2)