trait Eq[A] {
  def eqv(x: A, y: A): Boolean
}

def isEqual[A: Eq](x: A, y: A) = implicitly[Eq[A]].eqv(x, y)

implicit val intEq = new Eq[Int] {
  override def eqv(x: Int, y: Int): Boolean = x == y
}

implicit val stringEq = new Eq[String] {
  override def eqv(x: String, y: String): Boolean = x == y
}

implicit def optEq[A: Eq] = new Eq[Option[A]] {
  override def eqv(xOpt: Option[A], yOpt: Option[A]): Boolean = (xOpt, yOpt) match {
    case (Some(x), Some(y)) => implicitly[Eq[A]].eqv(x, y)
    case _ => false
  }
}

isEqual(Option.empty[Int], Option.empty[Int])
// res0: Boolean = false

isEqual(1, 1) == isEqual(Option(1), Option(1))
// res1: Boolean = true
isEqual(14, 88) == isEqual(Option(14), Option(88))
// res1: Boolean = true

isEqual("Foo", "Foo") == isEqual(Option("Foo"), Option("Foo"))
// res3: Boolean = true
isEqual("Foo", "Bar") == isEqual(Option("Foo"), Option("Bar"))
// res4: Boolean = true

