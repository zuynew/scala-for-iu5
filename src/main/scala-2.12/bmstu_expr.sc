// Выражение (sic!)
1 + 4

// Константа
val a = 8

// Переменная
var b = 8

// -----

// Функция
def inc(i: Int): Int = i + 1
// inc: inc[](val i: Int) => Int

def multiLineFunc(x: Int, y: Int) = {
  println(x, y)
  (y, x)
}
// multiLineFunc[](val x: Int,val y: Int) => (Int, Int)

val res = inc(2)
// res: Int = 3

// Lambda
val f1 = (i: Int) => i + 1
// f1: Int => Int

f1(2)
// res1: Int = 3

val f0 = inc _
// f0: Int => Int


/// ----

def adder(m: Int, n: Int) = m + n
// adder: adder[](val m: Int,val n: Int) => Int

val add2 = adder(2, _: Int)
// add2: Int => Int

add2(3)
// res2: Int = 5

def mul(m: Int)(n: Int): Int = m * n
// mul: mul[](val m: Int)(val n: Int) => Int

mul(2)(3)
// res3: Int = 6

val mulC = mul(2) _
// mulC: Int => Int

val add3 = (adder _).curried
// add3: Int => (Int => Int)

// ---


def bigger(o: Any): Any = {
  o match {
    case i: Int if i < 0 => i - 1
    case i: Int => i + 1
    case d: Double if d < 0.0 => d - 0.1
    case d: Double => d + 0.1
    case text: String => text + "s"
  }
}

bigger(-4)
// res1: Any = -5
bigger("foo")
// res2: Any = foos