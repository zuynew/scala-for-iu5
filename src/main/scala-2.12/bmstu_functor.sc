import cats._

implicit val optionFunctor: Functor[Option] = new Functor[Option] {
  def map[A, B](fa: Option[A])(f: A => B) = fa map f
}

implicit val listFunctor: Functor[List] = new Functor[List] {
  def map[A, B](fa: List[A])(f: A => B) = fa map f
}

implicit def function1Functor[In]: Functor[Function1[In, ?]] =
  new Functor[Function1[In, ?]] {
    def map[A, B](fa: In => A)(f: A => B): Function1[In, B] = fa andThen f
  }

Functor[List].map(List("qwer", "adsfg"))(_.length)
// res0: List[Int] = List(4, 5)

Functor[Option].map(Option("Hello"))(_.length)
// Functor[Option].map(Option("Hello"))(_.length)
Functor[Option].map(None: Option[String])(_.length)
// res2: Option[Int] = None


val listOpt = Functor[List] compose Functor[Option]
listOpt.map(List(Some(1), None, Some(3)))(_ + 1)
// res3: List[Option[Int]] = List(Some(2), None, Some(4))

