import cats._

implicit val optionApply: Apply[Option] = new Apply[Option] {
  def ap[A, B](f: Option[A => B])(fa: Option[A]): Option[B] =
    fa.flatMap(a => f.map(ff => ff(a)))

  def map[A, B](fa: Option[A])(f: A => B): Option[B] = fa map f

  override def product[A, B](fa: Option[A], fb: Option[B]): Option[(A, B)] =
    fa.flatMap(a => fb.map(b => (a, b)))
}

implicit val listApply: Apply[List] = new Apply[List] {
  def ap[A, B](f: List[A => B])(fa: List[A]): List[B] =
    fa.flatMap(a => f.map(ff => ff(a)))

  def map[A, B](fa: List[A])(f: A => B): List[B] = fa map f

  override  def product[A, B](fa: List[A], fb: List[B]): List[(A, B)] =
    fa.zip(fb)
}

val intToString: Int => String = _.toString

Apply[Option].ap(Some(intToString))(Some(1))
// res0: Option[String] = Some(1)
Apply[Option].ap(Some(intToString))(None)
// res1: Option[String] = None
Apply[Option].ap(None)(Some(1))
// res2: Option[Nothing] = None

val listOpt = Apply[List] compose Apply[Option]
val plusOne = (x: Int) => x + 1
listOpt.ap(List(Some(plusOne)))(List(Some(1), None, Some(3)))
// res3: List[Option[Int]] = List(Some(2), None, Some(4))

import cats.implicits._

(Option(1) |@| Option(2)) map {_ * _}
// res4: Option[Int] = Some(2)
(Option(1) |@| none[Int]) map {_ * _}
// res5: Option[Int] = None

Applicative[Option].pure(1)
// res6: Option[Int] = Some(1)

(Applicative[List] compose Applicative[Option]).pure(1)
// res7: List[Option[Int]] = List(Some(1))
