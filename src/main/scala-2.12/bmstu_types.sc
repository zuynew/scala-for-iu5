trait Cache[K, V] {
  def get(key: K): Option[V]
  def put(key: K, value: V)
  def delete(key: K)
}

def id[A](a: A) = a
// id: id[A](val a: A) => A

val idInt = id[Int] _
// idInt: Int => Int

val idString = id[String] _
// idString: String => String

id(1) == idInt(1)
id("Foo") == idString("Foo")

import scala.collection.mutable

class MutableMapCache[K, V]() extends Cache[K, V] {

  protected val map: mutable.Map[K, V] = mutable.Map[K, V]()

  override def get(key: K): Option[V] = map.get(key)

  override def put(key: K, value: V): Unit = map.put(key, value)

  override def delete(key: K): Unit = map.remove(key)
}


val cache: Cache[String, String] = new MutableMapCache[String, String]