import cats._

implicit def optionMonad(implicit app: Applicative[Option]) =
  new Monad[Option] {
    // Define flatMap using Option's flatten method
    override def flatMap[A, B](fa: Option[A])(f: A => Option[B]): Option[B] =
      app.map(fa)(f).flatten
    // Reuse this definition from Applicative.
    override def pure[A](a: A): Option[A] = app.pure(a)

    override def tailRecM[A, B](a: A)(f: (A) => Option[Either[A, B]]): Option[B] = ???
  }

implicit val listMonad = new Monad[List] {
  def flatMap[A, B](fa: List[A])(f: A => List[B]): List[B] = fa.flatMap(f)
  def pure[A](a: A): List[A] = List(a)

  override def tailRecM[A, B](a: A)(f: (A) => List[Either[A, B]]): List[B] = ???
}


import cats.implicits._

Monad[List].flatMap(List(1, 2, 3))(x => List(x, x))